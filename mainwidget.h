#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QBoxLayout>
#include <QFormLayout>
#if QT_VERSION >= 0x050000
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#else
#include <QtGui/QGraphicsView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#endif




#include <QSystemTrayIcon>
#include <QIcon>
#include <QImage>
#include <QMessageBox>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>

#include "settings.h"
#include "mainwindow.h"
#include "infobox.h"

class MainWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainWidget(QWidget *parent = 0);
    ~MainWidget();

protected:
    QGraphicsScene * _graphicsScene;
    QImage _image;
    QPixmap _pixmap;
    QGraphicsPixmapItem _pixmapItem;
    QGraphicsView * _graphicsView;
    QLabel * _messageLabel;
    QPushButton * _closeButton;

    QLabel* _passwordErrorMessageLabel;
    //QLabel * _passwordLabel;
    //QLineEdit * _passwordLineEdit;

    QBoxLayout * _layout;
    QFormLayout* _formLayout;

    QSystemTrayIcon * _trayIcon;
    QIcon * _icon;

    Settings * _settings;
    MainWindow* _main_window;
    QString _qrcodeProfileImagePath;
    QString _qrcodeTmpProfileImagePath;
    QSettings s;


    InfoBox * _configurationInfoBox;
    InfoBox * _networkInfoBox;
    QPushButton *_ok;
    QPushButton *_abort;
    bool _warningStatus;

    //typedef bool (*RegisterSessionNotification)(HWND, DWORD);
    //RegisterSessionNotification WTSRegisterSessionNotification;

    void setupUI();

    #ifdef Q_OS_WIN32

        //virtual bool nativeEvent(const QByteArray &eventType, void *message, long *result);


    #endif

    #ifdef Q_OS_DARWIN
    bool eventFilter(QObject *obj, QEvent * event);

//private:
//    bool macEvent( EventHandlerCallRef caller, EventRef event );
    #endif

signals:
    void closeButtonClicked();
    void uiActivity();

    void setCredentials(QString username,QString password);
    void sendPassword(QString password);
    void settingsUpdated(QString un,QString pwd,QString cn, QString cmac);

public slots:
    void emitUpdatedSettings(QString un,QString pwd,QString cn, QString cmac);
    void hideWindow();
    void wrongUsernameOrPassword();
    //void showNetworkErrorMessage(QNetworkReply::NetworkError e);
    void showConfigurationErrorMessage();

    void showImageFromData(const QByteArray &img);
    void onCloseClicked();

    void showQRCode(const QByteArray &img,const QString &bustHash, const QString &tinyURL);
    void showBustedImage(const QByteArray &img);

    void bringToFront();
    void printWidgetScreenToFile();
    void deleteFile();
    Settings* getSettings();
};

#endif // MAINWIDGET_H
