#ifndef SLEEPWAKE_H
#define SLEEPWAKE_H

#include <AppKit/AppKit.h>
#include <QDebug>
#include "globaluieventmanager.h"



@interface SleepWake : NSObject

-(void)onSleepEventHandler:(NSNotification *) note;
-(void)onWakeEventHandler:(NSNotification *) note;
-(void)onLogOutOrPowerOff:(NSNotification *) note;
-(void) startListen;
-(void) setComputerName: (NSString *) c;

@end
#endif // SLEEPWAKE_H
