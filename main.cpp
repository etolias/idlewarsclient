#include <QApplication>
#include <QString>
#include <QIODevice>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPushButton>
#include <QVBoxLayout>
#include <QObject>
#include <QThread>

#include "mainwidget.h"
#include "globaluieventmanager.h"
#include "globaluieventmanagerosx.h"
#include "globaluieventmanagerwin.h"
#include "computeridle.h"
#include "timeoflastaction.h"
#include "network.h"
#include "settings.h"
#include "infobox.h"
#include "socketmanager.h"
#include "settingstester.h"

#ifdef Q_OS_DARWIN
#include "qmacapp.h"

#include "IOKit/IOKitLib.h"
#endif




int main(int argc, char *argv[])
{
    qDebug()<<"NEW COMPILATION";

    qDebug()<<"Before qapplication";
#ifdef Q_OS_WIN32
    GlobalUiEventManagerWIN& globalEventManager = GlobalUiEventManagerWIN::getInstance();
    QApplication a(argc, argv);
    qDebug()<<"Before startListening";
    globalEventManager.startListening();
    qDebug()<<"After startListening";
    //globalEventManager.sessionLock();

    globalEventManager.sessionLock();





#endif
    qDebug()<<"main 1\n";
#ifdef Q_OS_DARWIN
    //GlobalUiEventManagerOSX& globalEventManager = GlobalUiEventManagerOSX::getInstance();
    QMacApp a(argc, argv);

#endif
    qDebug()<<"After qapplication";
    //add data to the registry
    QCoreApplication::setOrganizationName("UoS");
    QCoreApplication::setOrganizationDomain("soton.ac.uk");
    QCoreApplication::setApplicationName("EcoScreenCatcher");
    QCoreApplication::setApplicationVersion("0.6");

    MainWidget w;
    QSettings s;

    SocketManager socketManager(QHostAddress("127.0.0.1"), 9876);
    TimeOfLastAction tola;
    Network n(0, "aicvm-et2e10.ecs.soton.ac.uk/EcoScreenCatcher");
    n.setSettings(s.value("username").toString(), s.value("password").toString(), s.value("computerName").toString(),s.value("mac").toString());


#ifdef Q_OS_WIN32
    QObject::connect(&globalEventManager, SIGNAL(sessionLock()), &n, SLOT(getQRCode()));
    QObject::connect(&globalEventManager, SIGNAL(sessionUnlock()), &n, SLOT(onActive()));
#endif


    qDebug()<<"Before SettingsTester\n";
    SettingsTester st(0);

    qDebug()<<"After SettingsTester";
    QObject::connect(&st, SIGNAL(wrongConfiguration(QString)),&n, SLOT(sendWrongSettings(QString)));
    st.checkSettings();
    //qDebug()<<"After attching the connect from wrongConfiguration and sendWrongSetting";

    //Network n(0, "127.0.0.1:8000");

    QObject::connect(&w, SIGNAL(settingsUpdated(QString,QString,QString,QString)), &n, SLOT(setSettings(QString,QString,QString,QString)));
    QObject::connect(&w, SIGNAL(setCredentials(QString,QString)), &n, SLOT(setCredentials(QString,QString)));
    //QObject::connect(&w, SIGNAL(GETRequest(QString)), &n, SLOT(GETRequest(QString)));
    QObject::connect(&w, SIGNAL(sendPassword(QString)), &n, SLOT(sendCredentials(QString)));
    QObject::connect(&n, SIGNAL(wrongUsernameOrPassword()), &w, SLOT(wrongUsernameOrPassword()));
    QObject::connect(&n, SIGNAL(configurationError()), &w, SLOT(showConfigurationErrorMessage()));
    QObject::connect(&n, SIGNAL(configurationError()), &n, SLOT(requestCofiguration()));
    QObject::connect(&n, SIGNAL(configurationError()), w.getSettings(), SLOT(stop_animation()));


    QObject::connect(&n, SIGNAL(newConfiguration(int,int)), &tola, SLOT(setConfiguration(int,int)));

    QObject::connect(&tola, SIGNAL(heartbeat()), &n, SLOT(sendStatus()));
    QObject::connect(&n, SIGNAL(bustedHeartbeat()), &w, SLOT(bringToFront()));

    QObject::connect(&n, SIGNAL(computerIdle(QByteArray,QString,QString)), &w, SLOT(showQRCode(QByteArray,QString,QString)));



    QObject::connect(&n, SIGNAL(computerBusted(QByteArray)), &w, SLOT(showBustedImage(QByteArray)));
    QObject::connect(&n, SIGNAL(computerActive()), &tola, SLOT(resetTimer()));
    QObject::connect(&n, SIGNAL(computerActive()), &w, SLOT(hideWindow()));
    QObject::connect(&n, SIGNAL(computerActive()), &w, SLOT(deleteFile()));

    QObject::connect(&n, SIGNAL(waitingToGetImage()), &w, SLOT(deleteFile()));


    QObject::connect(&w, SIGNAL(closeButtonClicked()), &n, SLOT(resetBusted()));

    // TODO: is the following line redundant with QObject::connect(&n, SIGNAL(computerActive()), &tola, SLOT(resetTimer())); ?
    //QObject::connect(&w, SIGNAL(closeButtonClicked()), &tola, SLOT(resetTimer()));

#ifdef Q_OS_DARWIN
    //QObject::connect(&a, SIGNAL(uiActivity()), &w, SIGNAL(uiActivity()));
#endif

    //QObject::connect(&w, SIGNAL(uiActivity()), &n, SLOT(onActive()));
    //QObject::connect(&globalEventManager, SIGNAL(uiActivity()), &n, SLOT(onActive()));

    // TODO: do we need to add the following??
    //QObject::connect(&globalEventManager, SIGNAL(uiActivity()), &tola, SLOT(resetTimer()));

    //The following line commented because on the mac when the computer goes to sleep mode program shows an error message
    //QObject::connect(&n, SIGNAL(connectionError(QNetworkReply::NetworkError)), &w, SLOT(showNetworkErrorMessage(QNetworkReply::NetworkError)));

    //QObject::connect(&tola, SIGNAL(computerIdle()), &n, SLOT(getQRCode()));


    QObject::connect(&socketManager, SIGNAL(connected()), &n, SLOT(getQRCode()));
    QObject::connect(&socketManager, SIGNAL(disconnected()), &n, SLOT(onActive()));



    //globalEventManager.startListening();
    tola.resetTimer();


    return a.exec();
}


