#ifndef NETWORK_H
#define NETWORK_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QAuthenticator>
#include <QDebug>
#include <QString>
#include <QNetworkProxyFactory>
#include <QCoreApplication>

#ifdef Q_OS_DARWIN

#include <IOKit/IOKitLib.h>
#include <IOKit/ps/IOPowerSources.h>
#include <CoreFoundation/CoreFoundation.h>

#endif

#ifdef Q_OS_WIN32
#include <qt_windows.h>
#include <winbase.h>
#endif

class Network : public QObject
{
    Q_OBJECT
public :
    static QString staticHostName;
protected:

    QString hostName;
    QString computerName;
    QString username;
    QString password;
    QString mac;
    QString os;
    QString configurationURL;
    QString bustHash;
    QString tinyURL;

    enum {ACTIVE, IDLE, BUSTED} status;


    QUrl statusURL() const;
    //QNetworkReply * _reply;
    //void onFinished();

signals:
    void computerBusted(QByteArray img);
    void computerIdle(QByteArray img, QString bustHash, QString tinyURL);
    void computerActive();

    void bustedHeartbeat();

    void wrongUsernameOrPassword();
    void connectionError(QNetworkReply::NetworkError);
    void configurationError();
    void newConfiguration(int, int);

    void waitingToGetImage();

public:
    explicit Network(QObject *parent = 0, QString hn="127.0.0.1", QString un="evtol");
    static QNetworkAccessManager& QNManager();
    static QNetworkReply* GETRequest(QString path);

public slots:
    void connectionErrorHappened (QNetworkReply::NetworkError code);
    void setSettings(QString un,QString pwd,QString cname,QString cmac);
    void setCredentials(QString un,QString paswd);
    void sendCredentials(QString pass);
    void provideAuthentication(QNetworkReply* qnr , QAuthenticator* qa);

    void sendStatus();

    void sendWrongSettings(QString configuration);
    void wrongSettingsReply();

    void getQRCode();
    void onActive();
    void resetBusted();
    bool checkIfUserLoggedIn();



    void requestCofiguration();
    void readDataFromHttp(QNetworkReply* qnr);
};

#endif // NETWORK_H
