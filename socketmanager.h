#ifndef SOCKETMANAGER_H
#define SOCKETMANAGER_H

#include <QObject>
#include <QObject>
#include <QtNetwork>

class SocketManager : public QObject
{
    Q_OBJECT
public:
    explicit SocketManager(QHostAddress host, quint16 port, QObject *parent = 0);
    ~SocketManager();
    void close();

protected:
    QTcpServer* _server;
    QTcpSocket* _socket;
    QHostAddress _host;
    quint16 _port;

signals:
    void connected();
    void disconnected();
public slots:
    
private slots:
    void onNewConnection();
    void closeSocket();
};

#endif // SOCKETMANAGER_H
