
#ifndef GLOBALUIEVENTMANAGEROSX_H
#define GLOBALUIEVENTMANAGEROSX_H
#include "globaluieventmanager.h"

class GlobalUiEventManagerOSX: public GlobalUiEventManager{
protected:
    explicit GlobalUiEventManagerOSX(GlobalUiEventManager * parent =0):GlobalUiEventManager(parent){}

public:
    static GlobalUiEventManagerOSX& getInstance()
    {
        static GlobalUiEventManagerOSX instance;
        return instance;
    }
    //void uiAccessed();
    void startListening();
};
#endif // GLOBALUIEVENTMANAGEROSX_H

