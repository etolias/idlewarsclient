#ifndef GLOBALUIEVENTMANAGER_H
#define GLOBALUIEVENTMANAGER_H
#include <QDebug>
#include <QObject>
#include <QString>
#include "timeoflastaction.h"
#include "network.h"
#include <QLineEdit>


class GlobalUiEventManager: public QObject{
    Q_OBJECT
protected:
    GlobalUiEventManager(GlobalUiEventManager const&);              // Don't Implement
    void operator=(GlobalUiEventManager const&); // Don't implement

    explicit GlobalUiEventManager(QObject * parent =0):QObject(parent){
    }

    //virtual void uiAccessed() = 0;
public:
    virtual void startListening() = 0;

    void emitUiActivity(){
        //qDebug()<<"ui Activity";
        emit uiActivity();
    }
    void emitSessionLock(){
        qDebug()<<"Before emiting Session Lock";
        emit sessionLock();
        qDebug()<<"After emiting Session Lock";
    }

    //    void receiveSleepNote(){
    //        qDebug()<<"SLEEEp!";
    //    }

signals:
    void uiActivity();
    void sessionUnlock();
    void sessionLock();
    //void sleepWakeEventHappend(QString);
    //void compUserBusted(QByteArray img);

};


#endif // GLOBALUIEVENTMANAGER_H
