#include "qmacapp.h"
#include <QDebug>
#include <Cocoa/Cocoa.h>

QMacApp::QMacApp(int argc, char* argv[]) :
    QApplication(argc, argv)
{
    addAppAsLoginItem();
}


bool QMacApp::macEventFilter ( EventHandlerCallRef /*caller*/, EventRef event)
{
    NSEvent *e = reinterpret_cast<NSEvent *>(event);
    if ( [e type] == NSKeyDown ||
         [e type] == NSKeyUp ||
         [e type] == NSLeftMouseDown ||
         [e type] == NSLeftMouseUp ||
         [e type] == NSRightMouseDown ||
         [e type] == NSRightMouseUp ||
         [e type] == NSMouseMoved ||
         [e type] == NSLeftMouseDragged ||
         [e type] == NSRightMouseDragged ||
         [e type] == NSMouseEntered ||
         [e type] == NSMouseExited )
    {
       //qDebug() << "macEventFilter UI";
        emit uiActivity();
//    } else {
//        qDebug() << "macEventFilter other";
    }
    return false;
}

void QMacApp::addAppAsLoginItem(){
    NSString * appPath = [[NSBundle mainBundle] bundlePath];

    // This will retrieve the path for the application
    // For example, /Applications/test.app
    CFURLRef url = (CFURLRef)[NSURL fileURLWithPath:appPath];

    // Create a reference to the shared file list.
        // We are adding it to the current user only.
        // If we want to add it all users, use
        // kLSSharedFileListGlobalLoginItems instead of
        //kLSSharedFileListSessionLoginItems
    LSSharedFileListRef loginItems = LSSharedFileListCreate(NULL,
                                                     kLSSharedFileListSessionLoginItems, NULL);
    if (loginItems) {
        //Insert an item to the list.
        LSSharedFileListItemRef item = LSSharedFileListInsertItemURL(loginItems,
                                                     kLSSharedFileListItemLast, NULL, NULL,
                                                     url, NULL, NULL);
        if (item){
            CFRelease(item);
                }
    }

    CFRelease(loginItems);
}
