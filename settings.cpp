#include "settings.h"
#include "ui_settings.h"
#include <QDesktopServices>
#include <QUrl>
#include <QNetworkRequest>
Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
    this->animation_Label = new QLabel(this);

    QFormLayout * layout = findChild<QFormLayout*>("formLayout");
    layout->addWidget(animation_Label);

    //QLineEdit * computerNameEdit = findChild<QLineEdit*>("computerNameLineEdit");
    //QLabel * computerNameLabel = findChild<QLabel*>("computerNameLabel");
    //computerNameEdit->hide();
    //computerNameLabel->hide();

    //QLineEdit * password = findChild<QLineEdit*>("passwordLineEdit");
    //QLabel * passwordNameLabel = findChild<QLabel*>("passwordLabel");

    //password->hide();
    //passwordNameLabel->hide();




}

Settings::~Settings()
{
    delete ui;
}

void Settings::on_pushButton_clicked()
{
    QDesktopServices::openUrl(QUrl("http://"+Network::staticHostName));

}

void Settings::on_pushButton_2_clicked()
{
    QMovie* qm2 = new QMovie(":/images/images/bar_loader.gif");

    if(qm2->isValid()){

        qDebug()<<"gif is valid";
        //animation_Label->setMovie(qm2);
        //animation_Label->show();

        QLabel* validation_label = findChild<QLabel*>("validate_label");

        validation_label->setMovie(qm2);
        validation_label->show();


        qm2->start();
        QLineEdit * username= findChild<QLineEdit*>("usernameLineEdit");

        QString un= username->text();
        this->reply = Network::GETRequest("/api/check_code/"+un);

        //connect (reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(connectionErrorHappened(QNetworkReply::NetworkError)));
        connect (reply, SIGNAL(metaDataChanged()), this, SLOT(connectionErrorHappened()));


        //QNetworkRequest req(QUrl("http://"+this->hostName+"/verifyuser/"));

        /*QString concatenated = username + ":" + password;
        QByteArray data = concatenated.toLocal8Bit().toBase64();
        QString headerData = "Basic " + data;
        req.setRawHeader("Authorization", headerData.toLocal8Bit());*/

        //QNetworkReply * reply=QNManager().get(req);


    }else{
        qDebug()<<"gif is NOT valid";
    }
}
void Settings::connectionErrorHappened (){
    QVariant statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    qDebug()<<"status Code"<<statusCode.toInt()<<"\n";
    QLabel* validation_label = findChild<QLabel*>("validate_label");
    if(statusCode.isValid()){
        int int_statusCode = statusCode.toInt();
        if(int_statusCode==200){
            //animation_Label->setText("OK");
            validation_label->setText("code OK! Now press save and go to the web page and start catching.");
        }else if (int_statusCode==404){
            //animation_Label->setText("Wrong code!");
            validation_label->setText("Wrong code!");
        }else{

            validation_label->setText("Problem with the server!");
        }
    }
    else{
        //animation_Label->setText("Wrong code!");
        validation_label->setText("There is a problem with a server!");
    }
}

void Settings::stop_animation(){
    this->animation_Label->movie()->stop();
    //this->animation_Label->setText();
}

void Settings::on_pushButton_3_clicked()
{
    //QDesktopServices::openUrl(QUrl("http://"+Network::staticHostName+"/catchacomputer/"));
    QDesktopServices::openUrl(QUrl("http://game.ecs.soton.ac.uk:8000/catchacomputer/"));
}
