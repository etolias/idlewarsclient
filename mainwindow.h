#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#ifdef Q_OS_WIN32
#include <windows.h>
//#include <wtsapi32.h>
//#pragma comment(lib,"WtsApi32.lib")
#endif
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void firstConfiguration();

private slots:
    void on_Settings_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
