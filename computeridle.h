#ifndef COMPUTERIDLE_H
#define COMPUTERIDLE_H

#include <QMainWindow>
#include <QByteArray>
#include <QEvent>
#include <QDebug>
#include <QTimer>
namespace Ui {
class ComputerIdle;
}

class ComputerIdle : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit ComputerIdle(QWidget *parent = 0);
    ~ComputerIdle();
//    void contextMenuEvent(QContextMenuEvent *event){
//        qDebug() << "event";
//    }
protected:

    void mousePressEvent(QMouseEvent *){qDebug() << "mouse press event";this->hideWindow();emit userAction();}
    void wheelEvent(QWheelEvent *){qDebug() <<"QWheelEvet";this->hideWindow();emit userAction();}
    void mouseReleaseEvent(QMouseEvent *){qDebug() << "Mouse relseaseEvent"; this->hideWindow();emit userAction();}

    //void moveEvent(QMoveEvent *){qDebug() << "void moveEvent mouse MOVEEEEE!!!";this->hideWindow();}

    //void mouseMoveEvent(QMouseEvent *){qDebug() << "mouse mov";this->hideWindow();}
    void keyPressEvent(QKeyEvent *){qDebug() << "key press";this->hideWindow();userAction();}
signals:
   void userAction();
public slots:
    void show_image_from_data(QByteArray img);
    void hideWindow(){
        this->hide();

    }
    void setComputerBusted(){
        this->isBusted=true;
    }
    void setComputerNotBusted(){
        this->isBusted=false;
    }
    void isMouseMoved();

private:
    Ui::ComputerIdle *ui;
    bool isBusted;
    QTimer *qt;
    int prevX;
    int prevY;
};

#endif // COMPUTERIDLE_H
