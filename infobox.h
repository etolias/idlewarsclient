#ifndef INFOBOX_H
#define INFOBOX_H

#include <QMessageBox>
#include <QDebug>
#include <QCloseEvent>
#include <settings.h>
class InfoBox : public QMessageBox
{
Q_OBJECT
signals:
    void closed();

public:
    InfoBox();
    virtual void closeEvent(QCloseEvent * e);
    virtual void done(int i);
};

#endif // INFOBOX_H
