#include "network.h"
#include <QScriptValue>
#include <QScriptEngine>
#include <QDateTime>

#ifdef Q_OS_DARWIN
#include <IOKit/ps/IOPSKeys.h>
#endif

#ifdef Q_OS_WIN32

#include <windows.h>
#include <QThread>
#include "globaluieventmanagerwin.h"

#endif

QString Network::staticHostName;

Network::Network(QObject *parent, QString hn, QString un) :
    QObject(parent)
{
    this->hostName= hn;
    Network::staticHostName = hn;
    //staticHostName = hn;
    this->username= un;
    this->configurationURL="";
    connect(&QNManager(),SIGNAL(finished(QNetworkReply *)),this,SLOT(readDataFromHttp(QNetworkReply*)));
    connect(&QNManager(), SIGNAL(authenticationRequired(QNetworkReply*,QAuthenticator*)),this, SLOT(provideAuthentication(QNetworkReply*,QAuthenticator *)));
    configurationURL="http://users.ecs.soton.ac.uk/et2e10/conf.json";
    //configurationURL="http://users.ecs.soton.ac.uk/et2e10/confSoton.json";
    status = ACTIVE;
    QNManager().proxyFactory()->setUseSystemConfiguration(true);
    this->os="unknown os";
    #if defined( Q_OS_LINUX )
        os = "linux";
        // TODO: more info?
    #elif defined( Q_OS_WIN32 )
        os = QString("windows %1").arg( QSysInfo::WindowsVersion );
    #elif defined( Q_OS_MAC )
        os = QString("mac os %1").arg( QSysInfo::MacintoshVersion );
    #endif


}

QNetworkAccessManager& Network::QNManager()
{
    static QNetworkAccessManager QNM;//=new QNetworkAccessManager();
    return QNM;
}


void Network::connectionErrorHappened (QNetworkReply::NetworkError code)
{   /*Bother the user only if a 404 error happened*/
    /*qDebug()<<"Qnetwork accessible: "<<this->QNManager().networkAccessible();
    qDebug() << "+++++++++++++++++++++++++++++++++++++++++++Connection ERROR happened CODE: "<<code;
    if (code ==QNetworkReply::HostNotFoundError||
        //code == QNetworkReply::TimeoutError||
        code ==QNetworkReply::ConnectionRefusedError ||
        code== QNetworkReply::RemoteHostClosedError //||
        //code == QNetworkReply::TemporaryNetworkFailureError||
        //code == QNetworkReply::ProxyConnectionRefusedError||
        //code == QNetworkReply::ProxyConnectionClosedError ||
        //code == QNetworkReply::ProxyNotFoundError||
        //code == QNetworkReply::ProxyTimeoutError||
        //code == QNetworkReply::UnknownNetworkError
            ){
        // if we sent an heartbeat..
        emit connectionError(code);
        qDebug()<<"Qnetwork accessible: "<<this->QNManager().networkAccessible();
        //if (this->QNManager().networkAccessible() == QNetworkAccessManager::Accessible){

        // do nothing



    } else */
    if (code == QNetworkReply::ContentNotFoundError  ||
               code == QNetworkReply::UnknownContentError ) {
        emit configurationError();

    } else {
        // do nothing
    }


}

void Network::setSettings(QString un,QString pwd,QString cname, QString cmac)
{
    qDebug() << "SETTING SET!!!";
    qDebug() << "un: "<< un <<" pqd: " << " cname: "<<cname<<" cmac: "<<cmac;

    this->username= un;
    this->password= pwd;
    this->computerName=un;
    this->mac=cmac;
}

void Network::setCredentials(QString un,QString paswd)
{
    this->username=un;
    this->password=paswd;
}

void Network::sendCredentials(QString pass)
{
    qDebug() <<"in send credentials^^^^^^^^^^^^^^^^^^^";
    this->password=pass;
    qDebug() << "assign password : " << pass;

    qDebug() << "==>http://"+this->hostName+"/verifyuser/";
    QNetworkRequest req(QUrl("http://"+this->hostName+"/verifyuser/"));

    QString concatenated = username + ":" + password;
    QByteArray data = concatenated.toLocal8Bit().toBase64();
    QString headerData = "Basic " + data;
    req.setRawHeader("Authorization", headerData.toLocal8Bit());
    QNetworkReply * reply=QNManager().get(req);
    connect (reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(connectionErrorHappened(QNetworkReply::NetworkError)));
}

QUrl Network::statusURL() const {
    //qDebug() << "http://"+hostName+"/rawinput/"+this->computerName+"/events/";
    return QUrl("http://"+hostName+"/rawinput/"+this->computerName+"/events/");
}
void Network::sendWrongSettings(QString configuration){
    qDebug()<<"&&&&&&&&&&&&&&&WrongSettings&&&&&&&&&&&&&&&&&";
    //https://game.ecs.soton.ac.uk//IdleWars/wrongconf/test/test/
    //"aicvm-et2e10.ecs.soton.ac.uk/IdleWars"
    //QUrl wrongSettingsURL = QUrl("http://"+hostName+"/wrongconf/"+this->computerName+"/"+configuration+"/");
    try{
    QUrl wrongSettingsURL = QUrl("http://"+hostName+"/wrongconf/");
    QNetworkRequest request(wrongSettingsURL);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
    QByteArray postData;
    postData.append("username").append("=").append(this->username);
    postData.append("&conf").append("=").append(configuration);

    QNetworkReply * reply = QNManager().post(request,postData);

    connect (reply, SIGNAL(finished()), this, SLOT(wrongSettingsReply()));
    }catch(...){
        qDebug()<<"Error sending wrong configuration data to server";
    }
}
void Network::wrongSettingsReply(){
    /*QByteArray result= reply->readAll();
    QString res(result);
    qDebug()<<"---------"<<res<<"-------------";
    */
    qDebug()<<"Success!!!!!!!!!!!!!!!!!!!!!!!!";
}

void Network::sendStatus()
{

    //qDebug()<<"status: "<<status;
    QString s="";
    // TODO: this is not an ideal place to emit -- find a better place?
    if (status == BUSTED) {
        //qDebug() << "emitting bustedHeartbeat";
        emit bustedHeartbeat();
        s="CI";
    }else if (status == IDLE){
        s= "CI";

    }
    else{
        s="CA";
    }
    //qDebug() << "EVENT: " << s;
    //QUrl url("http://"+hostName+":"+port+"/rawinput/comp1/events/");
    /*qDebug()<<"http://"+hostName+"/rawinput/"+this->computerName+"/events/";
    qDebug()<<"computer name: "<<this->computerName;
    qDebug()<<"username: "<<this->username;
    qDebug()<<"mac address: "<<this->mac;
    qDebug()<<"os:"<<this->os;
    qDebug()<<"version:"<<QCoreApplication::applicationVersion();
    */
    #ifdef Q_OS_DARWIN
    //qDebug()<<"in darwing code....\n";
    QString bat_percentage="";
    QString adapter = "";
    try{
        //CFDictionaryRef powerSourceDict = NULL;
        //qDebug()<<"**1\n";
        CFTypeRef info = IOPSCopyPowerSourcesInfo();
        //qDebug()<<"**2\n";
        CFArrayRef list = IOPSCopyPowerSourcesList(info);
        //qDebug()<<"**3\n";
        int batteries = CFArrayGetCount(list);
        //qDebug()<<"**4";

        if(batteries==0){
            qDebug()<<"MAC: no butteries detected";
            //If there is no batter put percentege to -1
            bat_percentage="-1";
        }else{
            //qDebug()<<"**5\n";
            int total = 0;
            for (int i = 0 ; i < batteries ; i++)
                {
                    CFDictionaryRef pSource = IOPSGetPowerSourceDescription(info, CFArrayGetValueAtIndex(list, i));
                    if (!pSource){
                        total+=0;
                        continue;
                    }

                    const void *psValue = (CFStringRef)CFDictionaryGetValue(pSource, CFSTR(kIOPSNameKey));

                    int curCapacity = 0;
                    int maxCapacity = 0;
                    int percent;

                    psValue = CFDictionaryGetValue(pSource, CFSTR(kIOPSCurrentCapacityKey));
                    CFNumberGetValue((CFNumberRef)psValue, kCFNumberSInt32Type, &curCapacity);

                    psValue = CFDictionaryGetValue(pSource, CFSTR(kIOPSMaxCapacityKey));
                    CFNumberGetValue((CFNumberRef)psValue, kCFNumberSInt32Type, &maxCapacity);

                    percent = (int)((double)curCapacity/(double)maxCapacity * 100);
                    total += percent;
                    printf ("powerSource %d of %d: percent: %d/%d %d\n", i, CFArrayGetCount(list), curCapacity, maxCapacity, percent);

                }
            bat_percentage = QString::number(total/batteries);
            //qDebug()<<"Battery Percentage: "<<bat_percentage;
        }
        CFDictionaryRef adapterDICT=IOPSCopyExternalPowerAdapterDetails();

        if (adapterDICT==NULL){
            //qDebug()<<"No adapter is connected\n";
            adapter="AD";
        }else{
            //qDebug()<<"Adapter is connected\n";
            adapter="AC";
        }

        /*for (int i = 0; i < batteries; i++) {
               CFDictionaryRef battery = IOPSGetPowerSourceDescription(info, CFArrayGetValueAtIndex(list, i));
               CFStringRef powerStateString =(CFStringRef)CFDictionaryGetValue(battery, CFSTR(kIOPSPowerSourceStateKey));
               qDebug()<<powerStateString<<"\n";
        }*/
    }catch(...){
        qDebug()<<"Exception occured with the code related to the compuer battery data\n";

    }

    #endif

    #ifdef Q_OS_WIN32
    QString adapter = "";
    QString bat_percentage="";
    try{
        SYSTEM_POWER_STATUS status;

        if(GetSystemPowerStatus(&status)){
            int bat = status.BatteryLifePercent;
            bat_percentage = QString::number(bat);
            if (bat <0 || bat>100){
                bat_percentage = "-1";

            }
            int bt_flag= status.BatteryFlag;

            if( bt_flag == 128 ){
                bat_percentage = "-1";
            }
            int aclnst= status.ACLineStatus;
            //qDebug()<<"bat: "<<bat<<"\n";
            //qDebug()<<"ACLineStatus: "<<aclnst<<"\n";
            if(aclnst==0){
                qDebug()<<"Power supply NOT connected\n";
                adapter="AD";

            }else if (aclnst==1){
                //qDebug()<<"Power supply connected\n";
                adapter="AC";
                if(bat_percentage == "-1"){
                    //I put this just for consistesy with the mac version.
                    //In the mac version whenever there is no batery (e.g. on a desktop machine) the api returns AD
                    adapter="AD";
                }
            }else{

                qDebug()<<"UNKNOWN STATUS\n";
            }
        }else{
            qDebug()<<"Battery status failed";

        }
    }catch(...){
        qDebug()<<"Exception occured with the code related to the compuer battery data\n";
    }
    #endif
    QNetworkRequest request(this->statusURL());
    QByteArray postData;
    postData.append("eventType").append("=").append(s);
    postData.append("&user").append("=").append(this->username);
    postData.append("&mac").append("=").append(this->mac);
    postData.append("&appver").append("=").append(QCoreApplication::applicationVersion());
    postData.append("&os").append("=").append(this->os);
    postData.append("&clientTimestamp").append("=").append(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz"));

    postData.append("&adapter").append("=").append(adapter);
    postData.append("&battery_percentage").append("=").append(bat_percentage);


    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
    QNetworkReply * reply=QNManager().post(request,postData);
    connect (reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(connectionErrorHappened(QNetworkReply::NetworkError)));
    //qDebug()<<postData<<"\n";
}

void Network::getQRCode()
{
    qDebug()<<"In get qrCode";
    if (this->status == ACTIVE) {
        qDebug()<<"the computer is ACTIVE";
        qDebug() << "GET QR code: "<<"http://"+this->hostName+"/uploads/qrcodes/"+username+".png";
        QNetworkRequest req(QUrl("http://"+this->hostName+"/uploads/qrcodes/"+username+".png"));
        QNManager().get(req);
        this->status = IDLE;
        qDebug() << "after getting the QRCode";
    } else {
        qDebug() << "ignoring QR code request";
    }
}

void Network::onActive()
{
    #ifdef Q_OS_WIN32
    QThread::sleep(5);//sleep for 5 secs
    if (GlobalUiEventManagerWIN::is_comp_screen_locked){
        //if the computer screen is locked do not change the computer status until it is active again.
        qDebug()<<"on active::is_comp_screen_locked=true";
        return;
    }else{

        qDebug()<<"on active::is_comp_screen_locked=false";
    }
    //qDebug() << "Network::on_active";
    QThread::sleep(5);//sleep for 5 secs

    if (GlobalUiEventManagerWIN::is_comp_screen_locked){
        //if the computer screen is locked do not change the computer status until it is active again.
        qDebug()<<"on active::is_comp_screen_locked=true";
        return;
    }else{

        qDebug()<<"on active::is_comp_screen_locked=false";
    }
    #endif
    if (this->status == IDLE) {
        //qDebug() << "Network::on_active and we are IDLE";
        emit computerActive();
        //qDebug()<<"network::on_active.bustedEnded";
        this->status = ACTIVE;

    }else if (this->status==BUSTED){
        this->status = ACTIVE;
        emit computerActive();

    }
    if (this->status==ACTIVE){
        emit computerActive();
    }
}

void Network::resetBusted()
{
    if (this->status == BUSTED) {
        qDebug() << "resetBusted";
        this->status = ACTIVE;
        emit computerActive();
    } else {
        //throw "ERROR!!!!";
        qDebug() << "----------------------------------------------------";
        qDebug() << "ERROR: resetBusted should not be called from here";
        qDebug() << "----------------------------------------------------";
    }
}

bool Network::checkIfUserLoggedIn(){
    return true;

}

QNetworkReply* Network::GETRequest(QString path)
{
      //QNetworkRequest
      qDebug()<<staticHostName+path<<"\n";
      QNetworkRequest req(QUrl("http://"+staticHostName+path));

      QNetworkReply * reply = QNManager().get(req);
      return reply;
      //connect (reply, SIGNAL(error(QNetworkReply::NetworkError)), Network, SLOT(connectionErrorHappened(QNetworkReply::NetworkError)));
}

void Network::requestCofiguration()
{
    //GETRequest();
    QNetworkRequest req(configurationURL);

    QNetworkReply * reply = QNManager().get(req);
    connect (reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(connectionErrorHappened(QNetworkReply::NetworkError)));
    //connect (reply, SIGNAL(finished()), this, SLOT(onFinished()));
    //qDebug()<<"+++++++++++++++++++new Configuration requested";
}

//void Network::onFinished()
//{

//    return;
//}

void Network::provideAuthentication(QNetworkReply* qnr , QAuthenticator* qa)
{
    qDebug() << "+++++++++++++++++++++++++provideAuthentication++++++++++++++++++++++++++++++";
    qDebug() << qnr->readAll();
    qa->setUser(QString ("evtol"));
    //TODO:Invoke setCredentials slot to set username and password
    qDebug() << "setting password :" << password;
    qa->setPassword(password);
}

//    void checkUserCredentials(QString username, QString password){
//        qDebug() << "+++++++++++++++++Checking user credentials++++++++++++++++++";
//        QNetworkRequest req(QUrl("http://"+this->hostName+"/verifyuser/"));
//        QNetworkReply * reply=QNManager().get(req);
//        connect (reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(connectionErrorHappened(QNetworkReply::NetworkError)));
//    }

void Network::readDataFromHttp(QNetworkReply* qnr)
{
    //qDebug()<<"##########***************Data received*******************###########";

    QVariant contentTypeHeaderValue= qnr->header(QNetworkRequest::ContentTypeHeader);

    QByteArray www_authenticate;
    www_authenticate.append("WWW-Authenticate");

    QByteArray basic;
    basic.append("Basic");

    QByteArray WWW_authenticateHeaderValue = qnr->rawHeader(www_authenticate);

    QByteArray result= qnr->readAll();

    QVariant statusCode = qnr->attribute(QNetworkRequest::HttpStatusCodeAttribute);

    if (qnr->error() != QNetworkReply::NoError) {
        //qDebug() << "we have an error:" << qnr->error();
        if (qnr->error() != QNetworkReply::ProtocolUnknownError) {
            //qDebug() << "we have an error that is NOT: QNetworkReply::ProtocolUnknownError" << qnr->error() << "|" << QNetworkReply::ProtocolUnknownError;
            if (qnr->url() == this->statusURL()) {
                //
                requestCofiguration();
                qnr->deleteLater();
                return;
            }
        }
    }

    if (statusCode == 201){
    }

    QNetworkRequest qnreq=qnr->request();
    //qDebug() <<"*********************" << qnreq.url().path()<<"*********************";
    //qDebug() <<"status code: "<< statusCode;
    //qDebug() <<"contentTypeHeaderValue: "<< contentTypeHeaderValue.toString();
    if (contentTypeHeaderValue=="image/png" || contentTypeHeaderValue=="image/jpeg"){

        if (qnreq.url().path().startsWith("/EcoScreenCatcher/uploads/qrcodes/")){
            //Zero indicates that the computer is idle.
            emit computerIdle(result,this->bustHash,this->tinyURL);
            this->tinyURL="";

            //this->status = IDLE;
        }
        if (qnreq.url().path().startsWith("/EcoScreenCatcher/uploads/profileImg/")){
            this->status = BUSTED;
            qDebug() << "going BUSTED!";
            //one indicates that the computer is busted.
            emit computerBusted(result);
        }
        if (qnreq.url().path().endsWith("/picture")){
            this->status = BUSTED;
            qDebug() << "going BUSTED!";
            //one indicates that the computer is busted.
            emit computerBusted(result);
        }
    }
    else if (contentTypeHeaderValue=="text/html"){

        //qDebug()<<"html page received";
        //qDebug() << QString(result);
        if (statusCode==500 || statusCode==404){
            //TODO show message here.

            //qDebug() << "Emiting configuration error!!!";
            emit configurationError();
        }
    }
    else if (contentTypeHeaderValue=="application/json"){
        //qDebug() << "Parse Javascript";
        QScriptValue sc;
        QScriptEngine engine;
        sc= engine.evaluate("("+QString(result)+")");

        //qDebug()<<sc.toVariant();

        //qDebug()<<result;

        if(sc.isObject()){
            if(!sc.property("imgURL").isNull() && sc.property("imgURL").toString()!=""){
                if (this->status == IDLE) {
                    QString out = sc.property("imgURL").toString();

                    QUrl url(out);

                    QNetworkRequest requestImg(url);
                    //==>>>>>>>>>>here<<<<<<<<<<<<<==
                    emit waitingToGetImage();
                    //request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
                    QNetworkReply * reply=QNManager().get(requestImg);
                    connect (reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(connectionErrorHappened(QNetworkReply::NetworkError)));
                }
            }if(!sc.property("type").isNull() && sc.property("type").toString()=="authentication"){

                if(sc.property("status").toString()=="success"){
                    //Password Success
                    //qDebug() << "PASSWORD Success";
                    //emit hideWindow();
                }
                if(sc.property("status").toString()=="failure"){
                    //qDebug() << "PASSWORD Failure";
                    emit wrongUsernameOrPassword();
                }
            }if(!sc.property("bustHash").isNull()){
                this->bustHash=sc.property("bustHash").toString();
                //get the tiny url version of it.
                QNetworkRequest req(QUrl("http://tinyurl.com/api-create.php?url=http://"+this->hostName+"/bust/"+this->bustHash));
                QNManager().get(req);
            }
        }
    } else if (contentTypeHeaderValue=="text/plain; charset=UTF-8"){
        //qDebug()<<"++++++++++++++++++++++reading configuration";
        QScriptValue sc;
        QScriptEngine engine;

        //qDebug() << " result "<<QString(result);

        sc= engine.evaluate("("+QString(result)+")");

//        qDebug()<<sc.toVariant();

//        qDebug()<<result;

        if(sc.isObject()){
            if(!sc.property("hostname").isNull() && sc.property("hostname").toString()!="" &&
                    !sc.property("heartbeat").isNull() && sc.property("heartbeat").toString()!="" &&
                    !sc.property("milsecConsideredIdle").isNull() && sc.property("milsecConsideredIdle").toString()!=""){
                this->hostName = sc.property("hostname").toString();
                int hearbeat =  sc.property("heartbeat").toString().toInt();
                int milsecConsideredIdle = sc.property("milsecConsideredIdle").toString().toInt();
//                qDebug() << "±±±±±±±±±±±±±±±±±±±±±±±±EMMITTING NEW COFIGURATION±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±";
//                qDebug() << "HEARTBEAT="<<hearbeat;
//                qDebug() << "milisecConsideredIdle="<<milsecConsideredIdle;

                emit newConfiguration(hearbeat,milsecConsideredIdle);

            }
        }
    }
    else if (contentTypeHeaderValue=="text/plain"){
        this->tinyURL=QString(result);
    }
    qnr->deleteLater();
    return;
}
