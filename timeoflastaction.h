#ifndef TIMEOFLASTACTION_H
#define TIMEOFLASTACTION_H

#include <QObject>
#include <QDateTime>
#include <QTimer>
#include <QDebug>
//#include <QNetworkAccessManager>
//#include <QNetworkReply>
//#include <QUrl>
//#include <QScriptValueIterator>
//#include <QScriptEngine>
//#include <QAuthenticator>
//#include <globaluieventmanager.h>

class TimeOfLastAction : public QObject
{
    Q_OBJECT

protected:
    QDateTime lastActionDateTime;
    QTimer * timer;
    int milsecConsideredIdle;//if 10 sec is considered

    bool isConsideredIdle();

public:
    explicit TimeOfLastAction(QObject *parent = 0);
    ~TimeOfLastAction();
    //void updateEndDateOfIdle();
    void registerEventProxy(QString);//a method that by invocation emits a register Event signal
                                //The reason for this is that signals cannot be emited by mm files.

public slots:
    void checkTimePassedFromLastAction();
    void resetTimer();
    void setConfiguration(int hearBeat,int milisecondsConsideredIlde);

signals:
   void heartbeat();
   void computerIdle();
   //void actionHappened();
   //void compUserBusted(QByteArray);

};

#endif // TIMEOFLASTACTION_H
