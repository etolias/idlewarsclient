#ifndef QMACAPP_H
#define QMACAPP_H

#include <QApplication>

class QMacApp : public QApplication
{
    Q_OBJECT
public:
    explicit QMacApp(int argc, char* argv[]);

private:
    bool macEventFilter ( EventHandlerCallRef caller, EventRef event );
    void addAppAsLoginItem();
signals:
    void uiActivity();

public slots:

};

#endif // QMACAPP_H
