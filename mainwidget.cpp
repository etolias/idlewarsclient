#include "mainwidget.h"
#include "globaluieventmanagerwin.h"
#include <QCoreApplication>
#include <QMenu>
#include <QApplication>
#include <QDir>
#include <QLibrary>

MainWidget::MainWidget(QWidget *parent) :
    QWidget(parent)
{
    setupUI();
    //this->setFocusPolicy(Qt::StrongFocus);
#ifdef Q_OS_DARWIN
    QCoreApplication::instance()->installEventFilter(this);
#endif

    _graphicsScene = new QGraphicsScene(this);

    this->_settings=new Settings(this);
    this->_main_window = new MainWindow(this);
    if (s.value("username").toString()==""){
        this->_main_window->showNormal();
    }
    this->_configurationInfoBox= new InfoBox();
    this->_networkInfoBox = new InfoBox();
    #ifdef Q_OS_DARWIN
        this->_qrcodeProfileImagePath="/tmp/EcoScreenCatcher_image.png";
        this->_qrcodeTmpProfileImagePath="/tmp/EcoScreenCatcher_image_tmp.png";
    #endif

    #ifdef Q_OS_WIN32
        this->_qrcodeProfileImagePath=QDir::homePath()+"/EcoScreenCatcher_image.png";
        this->_qrcodeTmpProfileImagePath=QDir::homePath()+"/EcoScreenCatcher_image_tmp.png";

    #endif

    connect(_configurationInfoBox, SIGNAL(closed()), _settings, SLOT(showNormal()));
    connect(_networkInfoBox, SIGNAL(closed()), _settings, SLOT(showNormal()));

    setWindowTitle(tr("Wars"));

    QAction * quitAction= new QAction("Quit", this);
    QAction * settingsAction = new QAction("Settings",this);

    QMenu * trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(settingsAction);
    trayIconMenu->addAction(quitAction);

    connect(_settings, SIGNAL(settingsUpdated(QString,QString,QString,QString)), this, SLOT(emitUpdatedSettings(QString,QString,QString,QString)));
    connect(quitAction, SIGNAL(triggered()), QCoreApplication::instance(), SLOT(quit()));
    connect(settingsAction, SIGNAL(triggered()), _settings, SLOT(showNormal()));
    connect(_main_window,SIGNAL(firstConfiguration()), _settings, SLOT(showNormal()));

    _trayIcon = new QSystemTrayIcon(this);
    _trayIcon->setContextMenu(trayIconMenu);

    _icon= new QIcon(":img/images/eye.svg");
    _trayIcon->setIcon(*_icon);

    _trayIcon->show();

    this->setMouseTracking(true);

    setAttribute(Qt::WA_ShowWithoutActivating);
    _ok = _configurationInfoBox->addButton(QMessageBox::Ok);
    //_abort = _configurationInfoBox->addButton(QMessageBox::Abort);

    _abort = _configurationInfoBox->addButton(tr("Quit"),QMessageBox::RejectRole);

    //gview= findChild<QGraphicsView*>("graphicsView");
    //this->show();
    #ifdef Q_OS_WIN32


    /*if (WTSRegisterSessionNotification((HWND)this->winId(),NOTIFY_FOR_THIS_SESSION)){
        qDebug()<<"$$$$$$$$$$$$$$$$$True$$$$$$$$$$$$$$$$$$$$$$$$$$";
    }else{

        qDebug()<<"$$$$$$$$$$$$$$$$$$$$$$$$$$False$$$$$$$$$$$$$$$$$$$$$$$$$$";
    }*/

    //QLibrary wtsapi32Lib("wtsapi32");
    //wtsapi32Lib.load();
    //wtsapi32Lib.LoadHints();

//    if(wtsapi32Lib.isLoaded()){
    //WTSRegisterSessionNotification(FindWindowEx());

    //QLibrary user32Lib("user32");

        /*user32Lib.load();
        user32Lib.LoadHints();

        if(user32Lib.isLoaded()){
            RegisterSessionNotification WTSRegisterSessionNotification  = (RegisterSessionNotification)wtsapi32Lib.resolve("WTSRegisterSessionNotification");
            if(WTRegisterSessionNotification){
                WTSRegisterSessionNotification(MessageWindow::winId(),0);

            }
        }
    //}
    */

    #endif
}

MainWidget::~MainWidget() {
    delete _icon;
    //delete _graphicsScene;
    //delete _graphicsView;
    //delete _messageLabel;
    //delete _closeButton;
    //delete _passwordErrorMessageLabel;
    //delete _passwordLabel;
    //delete _passwordLineEdit;
    delete _formLayout;
    delete _trayIcon;
    delete _settings;
    delete _main_window;
    delete _configurationInfoBox;
    delete _networkInfoBox;
}

void MainWidget::setupUI(){
    //this->resize(452, 367);

    _graphicsView = new QGraphicsView(this);
    _graphicsView->setObjectName(QString::fromUtf8("graphicsView"));
    _graphicsView->setMouseTracking(false);
    QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    sizePolicy.setHeightForWidth(_graphicsView->sizePolicy().hasHeightForWidth());

    _messageLabel = new QLabel(QApplication::translate("ComputerIdle", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-weight:600; color:#ff0000;\">Bust this idle computer</span> by scanning the QR code below</p></body></html>", 0), this);
    _passwordErrorMessageLabel =new  QLabel(QApplication::translate("ComputerIdle", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-weight:600; color:#ff0000;\">Wrong password</span></p></body></html>", 0),this);
    _passwordErrorMessageLabel->hide();
    _formLayout= new QFormLayout();
    //_passwordLabel= new QLabel ("Password:", this);
    //_passwordLabel->hide();
    //_passwordLineEdit = new QLineEdit(this);
    //_passwordLineEdit->hide();
    //_passwordLineEdit->setEchoMode(QLineEdit::Password);

    //_formLayout->addRow(_passwordLabel,_passwordLineEdit);


    _graphicsView->setMouseTracking(true);

    _closeButton = new QPushButton(this);
    _closeButton->setObjectName(QString::fromUtf8("Close"));
    _closeButton->setText(QString::fromUtf8("Close"));
    connect(_closeButton, SIGNAL(clicked()), this, SLOT(onCloseClicked()));

    _layout = new QVBoxLayout(this);
    _layout->setSpacing(3);
    _layout->setObjectName(QString::fromUtf8("layoyt"));

    _layout->addWidget(_messageLabel);
    _layout->addWidget(_graphicsView);
    _layout->addWidget(_passwordErrorMessageLabel);
    _layout->addLayout(_formLayout);
    //_layout->insertLayout(_formLayout);
    //_layout->setLayout(_formLayout);
    _layout->addWidget(_closeButton);

}

// TODO: modify this so that it does not trigger when the window
// is moved or resized
#ifdef Q_OS_DARWIN
bool MainWidget::eventFilter(QObject * /*obj*/, QEvent * event){
    //qDebug() << " eventFilter!!!";
    if (event->type() == QEvent::MouseMove)
    {
        //qDebug() << "MainWidget::eventFilter";
        event->accept();
        emit uiActivity();
    }
    if (event->type()==QEvent::KeyPress){
        //qDebug() << "KeyPressed";
        event->accept();
        emit uiActivity();
    }
    return false;
}

//bool MainWidget::macEvent( EventHandlerCallRef caller, EventRef event )
//{
//    qDebug() << "macEvent";
//    return false;
//}

#endif

void MainWidget::emitUpdatedSettings(QString un,QString pwd,QString cn ,QString cmac){
    emit settingsUpdated(un,pwd,cn,cmac);
}

void MainWidget::hideWindow(){
    //qDebug()<<"MainWidget::hideWindow().Begining";
    this->hide();
    // qDebug()<<"MainWidget::hideWindow().End";
}

void MainWidget::wrongUsernameOrPassword(){
    //QLabel * label= findChild<QLabel*>("messageLabel");
    //label->setText("Wrong Username or Password");
    //label->setStyleSheet("QLabel { color : red; }");
}

//void MainWidget::showNetworkErrorMessage(QNetworkReply::NetworkError e){
//    qDebug() << e;
//    if (e!=QNetworkReply::UnknownContentError){
//        if(!_settings->isVisible()){
//            _networkInfoBox->setWindowTitle("Network");
//            _networkInfoBox->setText("Unable to connect to server!");
//            _networkInfoBox->setIcon(QMessageBox::Information);
//            _networkInfoBox->exec();
//        }

//        _warningStatus = false;
//    }
//}

void MainWidget::showConfigurationErrorMessage(){
    if(!_settings->isVisible() && !_main_window->isVisible()){
        qDebug() << "Show Configuration Error message";
        qDebug() << "configuration info box: is anebled? :"<< _configurationInfoBox->isEnabled();
        _configurationInfoBox->setWindowTitle("Configuration");
        _configurationInfoBox->setText("Client code may be incorrect, please check them!");
        _configurationInfoBox->setIcon(QMessageBox::Information);
        //_configurationInfoBox->setStandardButtons(QMessageBox::Abort|QMessageBox::Ok);


        //_configurationInfoBox->setDefaultButton(QMessageBox::Ok);
        int ret = _configurationInfoBox->exec();
        qDebug()<<_configurationInfoBox->clickedButton()<<"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Before Exiting application";
        qDebug()<<"Abort: "<<QMessageBox::Abort;
        if(_configurationInfoBox->clickedButton()==_abort){
            qDebug()<<"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Exiting application";
            qDebug()<<"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Exiting application";
            qDebug()<<"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Exiting application";
            this->close();
            QCoreApplication::exit(-1);
        }
    }
}

void MainWidget::showQRCode(const QByteArray& img, const QString& bustHash, const QString& tinyURL){
    //if(!GlobalUiEventManagerWIN::is_comp_screen_locked){
        _closeButton->hide();
        _messageLabel->setText("<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-weight:600; color:#ff0000;\">Bust this idle computer</span> by scanning the QR code below</p><p align=\"center\">Bust Code:"+bustHash+"</p><p align=\"center\">"+tinyURL+"</p></body></html>");
        showImageFromData(img);
    //}
}

void MainWidget::showBustedImage(const QByteArray& img){
    //_closeButton->show();
    //_passwordLineEdit->setText("");
    _messageLabel->setTextFormat(Qt::RichText);
    _messageLabel->setText("<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-weight:600; color:#ff0000;\">Next time please remember to turn your computer off! <br/> You have been busted by: </span></p></body></html>");
    showImageFromData(img);

    //_passwordLabel->show();
    //_passwordLineEdit->show();
}


void MainWidget::showImageFromData(const QByteArray& img){
    qDebug() <<"Trying to show the window";
    this->setWindowFlags(Qt::CustomizeWindowHint);

    qDebug() << "is it visible? ==>" << this->isVisible();

    _image.loadFromData(img);

    _pixmap = QPixmap::fromImage(_image);

    _pixmapItem.setPixmap(_pixmap);

    _graphicsScene->addItem(&_pixmapItem);

    QGraphicsView * gview= findChild<QGraphicsView*>("graphicsView");
    gview->setScene(_graphicsScene);

    this->showMaximized();

    QLineEdit * pass= findChild<QLineEdit*>("passwordLineEdit");
    //pass->setText("");

    //this->showFullScreen();
    this->setWindowState(this->windowState() & ~Qt::WindowMinimized | Qt::WindowActive);

    this->raise();
    this->activateWindow();
    gview->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    gview->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    gview->fitInView(0, 0, _graphicsScene->width(), _graphicsScene->height());
    gview->fitInView(&_pixmapItem, Qt::KeepAspectRatio);
    this->printWidgetScreenToFile();
    this->hideWindow();
    qDebug()<<"image has written to file";
}

void MainWidget::onCloseClicked()
{
    qDebug() << "MainWidget::on_Close_clicked";
    //if (_settings->set.value("password").toString()==_passwordLineEdit->text()){
    //    _passwordErrorMessageLabel->hide();
    //    _passwordLabel->hide();
    //    _passwordLineEdit->hide();
        this->hideWindow();
        emit closeButtonClicked();
    //}else{
    //    _passwordErrorMessageLabel->show();
    //}
}

void MainWidget::bringToFront()
{
    qDebug() << "MainWidget::bringToFront";
    this->activateWindow();
    this->raise();
}

Settings* MainWidget::getSettings(){

    return this->_settings;

}

void MainWidget::printWidgetScreenToFile(){
    //QString fileName = "/Users/user/tmp/EcoScreenCatcher_image.png";

    //if(!QPixmap::grabWidget(this).save(this->_qrcodeProfileImagePath, qPrintable("PNG")))
     QFile::remove(this->_qrcodeProfileImagePath);
#if QT_VERSION >= 0x050000
    if(this->grab().save(this->_qrcodeTmpProfileImagePath, qPrintable("PNG")))
    {

        QFile::rename(this->_qrcodeTmpProfileImagePath,this->_qrcodeProfileImagePath);
        // since you have a widget, just use grabWidget() here. winId() would possibly have
        // portability issues on other platforms.  qPrintable(saveExtension) is effectively
        // the same as saveExtension.toLocal8Bit().constData()
        //The following line is commented because it cause the applicaiton to close after pressing OKs
        //QMessageBox::warning(this, "File could not be saved", "ok", QMessageBox::Ok);
    }
#else
    if(QPixmap::grabWidget(this).save(this->_qrcodeProfileImagePath, qPrintable("PNG"))){
        QFile::rename(this->_qrcodeTmpProfileImagePath,this->_qrcodeProfileImagePath);
        //The following line is commented because it cause the applicaiton to close after pressing OKs
        //QMessageBox::warning(this, "File could not be saved", "ok", QMessageBox::Ok);
    }
#endif

}
void MainWidget::deleteFile(){
    QFile::remove(this->_qrcodeProfileImagePath);
    QImage img(":img/images/Loading.png");
    img.save(this->_qrcodeProfileImagePath);
}
#ifdef Q_OS_WIN32
    /*bool MainWidget::nativeEvent(const QByteArray &eventType, void *message, long *result){
        qDebug()<<"NativeEvent";
        qDebug()<<"eventType: "<<eventType<<"/n";
        qDebug()<<"message"<<message<<"/n";
        qDebug()<<"result"<<result<<"/n";
        return false;
    }*/
#endif
