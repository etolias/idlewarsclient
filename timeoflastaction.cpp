#include "timeoflastaction.h"

TimeOfLastAction::TimeOfLastAction(QObject *parent) :
    QObject(parent)
{
    timer = new QTimer(this);
    this->lastActionDateTime=QDateTime::currentDateTime();
    //this->milsecConsideredIdle=3*60*1000;
    this->milsecConsideredIdle=5*1000;
    int heartbeat = 2 * 1000;
    timer->setInterval(heartbeat);

    parent->connect(timer, SIGNAL(timeout()), this, SLOT(checkTimePassedFromLastAction()));

    timer->start();
}
TimeOfLastAction::~TimeOfLastAction(){
    delete timer;

}
void TimeOfLastAction::resetTimer(){
    //qDebug()<<"TimeOfLastAction::resetTimer()";
    this->lastActionDateTime=QDateTime::currentDateTime();
}

void TimeOfLastAction::checkTimePassedFromLastAction(){
    //qDebug() << "Is it considered Idle? "<<this->isConsideredIdle();

    emit heartbeat();

    /*if (this->isConsideredIdle()) {
        emit heartbeat("CI");
        //emit computerIdle();
        qDebug()<<"computer Idle Emited";
    }
    else{
        qDebug() << "TimeOfLastAction::checkTimePassedFromLastAction";
        emit heartbeat("CA");
    }*/
}

bool TimeOfLastAction::isConsideredIdle(){
    QDateTime now = QDateTime::currentDateTime();
    QDateTime limitDatetimeComputerConsideredActive = QDateTime(this->lastActionDateTime);
    limitDatetimeComputerConsideredActive = limitDatetimeComputerConsideredActive.addMSecs(this->milsecConsideredIdle);
    return limitDatetimeComputerConsideredActive < now;
}

void TimeOfLastAction::setConfiguration(int heartbeat, int msConsideredIlde){
    this->timer->setInterval(heartbeat);
    this->milsecConsideredIdle = msConsideredIlde;
}

/*void TimeOfLastAction::registerEventProxy(QString e){

    emit registerEvent(e);
}*/
