#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include <QDebug>
#include <QSettings>
#include <QLineEdit>
#include <QLabel>
#include <QMessageBox>
#include <QCloseEvent>
#include <QNetworkInterface>
#include <QMovie>
#include <network.h>

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT
    
public slots:
        void stop_animation();
        void connectionErrorHappened ();
public:
    explicit Settings(QWidget *parent = 0);
    virtual void reject(){
        this->hide();
    }
    virtual void accept(){
        qDebug() << "accept";



        try{
            QLineEdit * username= findChild<QLineEdit*>("usernameLineEdit");

            QString un= username->text();

            //QLineEdit * password = findChild<QLineEdit*>("passwordLineEdit");
            //QString pwd= password->text();

            //QLineEdit * computerName = findChild<QLineEdit*>("computerNameLineEdit");
            //QString cn = computerName->text();

            this->set.setValue("username", un);
            //this->set.setValue("password",pwd);
            //this->set.setValue("computerName",cn);



            qDebug() <<"username: " << set.value("username").toString();
            qDebug() <<"password: "<< set.value("password").toString();
            qDebug() <<"computerName"<<set.value("computerName").toString();
            qDebug() <<"mac"<<set.value("computerName").toString();
            this->hide();

            if (this->set.value("mac").toString() == ""){
                QString macAddress=getMacAddress();
                this->set.setValue("mac",macAddress);
                qDebug()<<"mac address: "<<macAddress;
            }




            //emit settingsUpdated(set.value("username").toString(),set.value("password").toString(),set.value("computerName").toString());
            emit settingsUpdated(set.value("username").toString(),set.value("password").toString(),set.value("username").toString(), set.value("mac").toString());
        }catch(int ){
            qDebug() << "problem storing data";
            msgBox.setText("setText");
            msgBox.exec();
            QMessageBox::information(this,"Settings!", "Unable to store settings!");

        }
    }

    QString getMacAddress() {
        QList<QNetworkInterface> interfaces = QNetworkInterface::allInterfaces();
        for(int i=0;i<interfaces.size();i++)
        {
            //QNetworkInterface interface = interfaces.at(i);
            //QNetworkInterface interface = interfaces.at(i);

            if( interfaces.at(i).isValid() )
            {

                QList<QNetworkAddressEntry> ipAddresses = interfaces.at(i).addressEntries();
                QString macAddress = interfaces.at(i).hardwareAddress();
                if( macAddress.length() > 0 && ipAddresses.size() >0 )
                {
                    return macAddress;
                }
            }

        }
        return "unknown";
    }

    virtual void showNormal(){
        qDebug()<<"in Shownormal";
        //QDialog::showNormal();
    }

    virtual void showEvent(QShowEvent * /*event*/){
        qDebug() <<"****ShowEvent****";

        QLineEdit * username= findChild<QLineEdit*>("usernameLineEdit");

        username->setText(set.value("username").toString());

        //QLineEdit * password = findChild<QLineEdit*>("passwordLineEdit");
        //password->setText(set.value("password").toString());

        //QLineEdit * computerName = findChild<QLineEdit*>("computerNameLineEdit");
        //computerName->setText(set.value("computerName").toString());
        this->setWindowState( (windowState() & ~Qt::WindowMinimized) | Qt::WindowActive);
        #ifdef Q_OS_DARWIN
            this->raise();
        #endif
    }

    virtual void closeEvent(QCloseEvent * event){
        qDebug() << "closeEvent" ;
        this->hide();
        event->ignore();
        return;

    }

    ~Settings();
    QSettings set;
signals:
    void settingsUpdated(QString un,QString pwd,QString cName, QString mac);
    
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();


    void on_pushButton_3_clicked();

private:
    Ui::Settings *ui;
    QMessageBox msgBox;
    QLabel * animation_Label;
    QMovie  movie;
    QNetworkReply* reply;
};

#endif // SETTINGS_H
