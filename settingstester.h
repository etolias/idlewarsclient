#ifndef SETTINGSTESTER_H
#define SETTINGSTESTER_H

#include <QObject>
#include <QSettings>
#include <QTimer>
class SettingsTester : public QObject
{
    Q_OBJECT
public:
    SettingsTester(QObject *parent = 0);
    ~SettingsTester();
    void get_platform_uuid(char * buf, int bufSize);
signals:
    void wrongConfiguration(QString configuration);
public slots:
    void checkSettings();
private:
    QTimer *timer;
};

#endif // SETTINGSTESTER_H
