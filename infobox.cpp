#include "infobox.h"

InfoBox::InfoBox()
{

}

void InfoBox::closeEvent(QCloseEvent * e){
    qDebug() << "close Event";
    this->hide();
    e->ignore();
    emit closed();
}

void InfoBox::done(int /*i*/){
    qDebug() << "done";
    this->hide();
    emit closed();
}
