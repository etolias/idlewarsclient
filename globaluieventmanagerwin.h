#ifdef _WIN32
#ifndef GLOBALUIEVENTMANAGERWIN_H
#define GLOBALUIEVENTMANAGERWIN_H

#include "globaluieventmanager.h"
#include <windows.h>
#include <winuser.h>
#include <windef.h>
#include <QDebug>


class GlobalUiEventManagerWIN : public GlobalUiEventManager
{

protected:
    explicit GlobalUiEventManagerWIN(GlobalUiEventManager *parent = 0):GlobalUiEventManager(parent){
         keyboardHook=NULL;
         mouseHook=NULL;
     }

public:
    static GlobalUiEventManagerWIN& getInstance()
    {
        static GlobalUiEventManagerWIN instance;
        return instance;
    }

    ~GlobalUiEventManagerWIN(){

        if (keyboardHook!=NULL){
            UnhookWindowsHookEx(keyboardHook);
            keyboardHook = NULL;
        }

        if (mouseHook!=NULL){

            UnhookWindowsHookEx(mouseHook);
            mouseHook= NULL;
        }

        if (sleepHook!=NULL){
            UnhookWindowsHookEx(sleepHook);
            sleepHook= NULL;
        }
    }

    //void uiAccessed();
    void startListening();


private:
    HHOOK keyboardHook;
    HHOOK mouseHook;
    HHOOK sleepHook;
    //WNDCLASSEX wcex;

    static LRESULT CALLBACK keyboardProcedure (int nCode,WPARAM wParam, LPARAM lparam );
    static LRESULT CALLBACK mouseProcedure (int nCode,WPARAM wParam, LPARAM lparam );
    //static LRESULT CALLBACK sleepProcedure (HWND hwnd, UINT uMsg,WPARAM wParam, LPARAM lParam);
public:
    static bool is_comp_screen_locked;

};
static LRESULT CALLBACK sleepProcedure (HWND hwnd, UINT uMsg,WPARAM wParam, LPARAM lParam);

#endif // GLOBALUIEVENTMANAGERWIN_H
#endif //OS
