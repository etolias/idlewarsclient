#-------------------------------------------------
#
# Project created by QtCreator 2013-05-28T22:09:18
#
#-------------------------------------------------

QT       += core gui
QT       += network
QT       += script
QT       += svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = EcoScreenCatcher
TEMPLATE = app


SOURCES += main.cpp\
    timeoflastaction.cpp \
    globaluieventmanager.cpp \
    network.cpp \
    settings.cpp \
    infobox.cpp \
    mainwidget.cpp \
    socketmanager.cpp \
    settingstester.cpp \
    mainwindow.cpp


HEADERS  += globaluieventmanager.h \
    globaluieventmanagerosx.h \
    timeoflastaction.h \
    #sleepwake.h \
    network.h \
    settings.h \
    infobox.h \
    mainwidget.h \
    socketmanager.h \
    settingstester.h \
    mainwindow.h


FORMS    += settings.ui \
    mainwindow.ui

win32{
    SOURCES += globaluieventmanagerWIN.cpp
    HEADERS += globaluieventmanagerwin.h
}
win32-msvc2008{
    LIBS += user32.lib
}

mac {
    HEADERS += qmacapp.h
    OBJECTIVE_SOURCES +=globaluieventmanagerOSX.mm \
                        qmacapp.mm
                        #sleepwake.mm
    LIBS += -framework AppKit \
            -framework IOKit

}


#OTHER_FILES += \
#    globaluieventmanager.mm

RESOURCES += \
    IdleWarsEventManager.qrc





