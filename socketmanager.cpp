#include "socketmanager.h"

SocketManager::SocketManager(QHostAddress host, quint16 port, QObject *parent) :
    QObject(parent)
{
    _socket = NULL;
    _server = new QTcpServer(this);
    _host = host;
    _port = port;

    connect(_server, SIGNAL(newConnection()), this, SLOT(onNewConnection()));
    //connect(_socket,SIGNAL(), this,)
    _server->setMaxPendingConnections(1);
    _server->listen(_host, _port);
}

SocketManager::~SocketManager() {
    delete _server;
    //delete _socket;
}

void SocketManager::onNewConnection() {
    _socket = _server->nextPendingConnection();
    connect(_socket, SIGNAL(disconnected()), this, SLOT(closeSocket()));
    if(_socket == 0) {
        return;
    }
    qDebug() << "new connection";
    emit connected();
}

void SocketManager::closeSocket() {
    qDebug()<<"SocketManager::closeSocket()";
    _socket->close();
    emit disconnected();
}

void SocketManager::close() {
    qDebug()<<"SocketManager::close()";
    if(_socket) {
        _socket->close();
    }
    if(_server) {
        _server->close();
    }
    //TODO: emit computer active here.

}
