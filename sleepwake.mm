#import "sleepwake.h"
#include <QDebug>

@implementation SleepWake
NSString* srvurl;
-(id) init{
    self= [super init];
    //QDebug::qDebut()<<"In init";
    srvurl=[NSString string];

    //[self startListen];
    return self;
}

-(void)onSleepEventHandler: (NSNotification *) note{
    qDebug()<<"******onSleepEventHandler************";

    //NSURL *url = [NSURL URLWithString:@"http://aicvm-et2e10.ecs.soton.ac.uk/IdleWars/rawinput/"+this->computerName+"/events/"];
     //NSURL *url = [NSURL URLWithString:@"http://aicvm-et2e10.ecs.soton.ac.uk/:80/rawinput/comp1/events/"];

     /*NSURL *url = [NSURL URLWithString:srvurl];
     NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
     [request setHTTPMethod:@"POST"];
     const char *bytes = "eventType=CH";
     [request setHTTPBody:[NSData dataWithBytes:bytes length:strlen(bytes)]];


     NSArray *keys = [NSArray arrayWithObjects:@"Content-Type", @"Accept-Charset", nil];
     NSArray *objects = [NSArray arrayWithObjects:@"application/x-www-form-urlencoded", @"ISO-8859-1,utf-8;q=0.7,*;q=0.3", nil];
     NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:objects
                                                            forKeys:keys];

     [request setAllHTTPHeaderFields:dictionary];

     NSURLResponse *response;
     NSError *err;
     NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    */
    printf("ON SLEEEEEEEEEEEEEEP EVENT HANDLER!!!");
}

-(void)onWakeEventHandler:(NSNotification *) note{
    qDebug()<<"******onWakeEventHandler************";
    //NSString* urlstr=[NSString stringWithFormat:@"%@%@%@",@"http://aicvm-et2e10.ecs.soton.ac.uk/IdleWars/rawinput/",cmpnm,@"/events/"];
    //NSURL *url = [NSURL URLWithString:@"http://aicvm-et2e10.ecs.soton.ac.uk/IdleWars/rawinput/comp1/events/"];
    //NSURL *url = [NSURL URLWithString:@"http://aicvm-et2e10.ecs.soton.ac.uk/IdleWars/rawinput/comp1/events/"];
    /*NSURL *url = [NSURL URLWithString:srvurl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    const char *bytes = "eventType=HW";
    [request setHTTPBody:[NSData dataWithBytes:bytes length:strlen(bytes)]];


    NSArray *keys = [NSArray arrayWithObjects:@"Content-Type", @"Accept-Charset", nil];
    NSArray *objects = [NSArray arrayWithObjects:@"application/x-www-form-urlencoded", @"ISO-8859-1,utf-8;q=0.7,*;q=0.3", nil];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:objects
                                                           forKeys:keys];

    [request setAllHTTPHeaderFields:dictionary];

    NSURLResponse *response;
    NSError *err;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    printf("ON SLEEEEEEEEEEEEEEP EVENT HANDLER!!!");
    */
    //qDebug()<<"onWakeEventHandler";
}

-(void)onLogOutOrPowerOff:(NSNotification *) note{
    qDebug()<<"******onWakeEventHandler************";

}
-(void) startListen{
    [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver: self
                                                        selector: @selector(onSleepEventHandler:)
                                                        name: NSWorkspaceWillSleepNotification
                                                        object: NULL];
    [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver: self
                                                        selector: @selector(onWakeEventHandler:)
                                                        name: NSWorkspaceDidWakeNotification
                                                        object: NULL];

    [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver: self
                                                        selector: @selector(onLogOutOrPowerOff:)
                                                        name: NSWorkspaceWillPowerOffNotification
                                                        object: NULL];

    qDebug()<<"EVENT Attached";

}
-(void) setURL: (NSString *) c{
    srvurl=c;

}
@end

