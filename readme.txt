# How to deploy on mac (dmg file) #

path were the files exists: /Users/user/code/QTCode/git/cmlRelease
rm *.o *.cpp *.h
mv IdleWarsEventManager.app IdleWars6.1.app

qmake /Users/user/code/QTCode/git/idlewarsclient/IdleWarsEventManager.pro -r -spec macx-g++ CONFIG+=release CONFIG+=x86_64 && make && make install

macdeployqt IdleWarsEventManager.app -verbose=2 -dmg

Go to iceberg and build the file

then run the following in the command line:

hdiutil create ./IdleWars.dmg -srcfolder ~/Dropbox/build/IdleWars.pkg -ov