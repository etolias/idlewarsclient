#include "settingstester.h"
#include <QDebug>
#include <QDir>

#ifdef Q_OS_DARWIN
#include "IOKit/IOKitLib.h"
void SettingsTester::get_platform_uuid(char * buf, int bufSize)
{
  io_registry_entry_t ioRegistryRoot = IORegistryEntryFromPath(kIOMasterPortDefault, "IOService:/");
  CFStringRef uuidCf = (CFStringRef) IORegistryEntryCreateCFProperty(ioRegistryRoot, CFSTR(kIOPlatformUUIDKey), kCFAllocatorDefault, 0);
  IOObjectRelease(ioRegistryRoot);
  CFStringGetCString(uuidCf, buf, bufSize, kCFStringEncodingMacRoman);
  CFRelease(uuidCf);
}
#endif

SettingsTester::SettingsTester (QObject *parent):
    QObject(parent)
{
    qDebug()<<"****************";
    qDebug()<<"Setting up timer";
    qDebug()<<"****************";

    timer= new QTimer(this);

    timer->setInterval(18000000);//Check every three hours
    //timer->setInterval(1000);//Check every three hours
    this->connect(timer,SIGNAL(timeout()),this,SLOT(checkSettings()));
    timer->start();
}
SettingsTester::~SettingsTester(){
    delete timer;
}

void SettingsTester::checkSettings()
{
    qDebug()<<"Checking Settings";
try{
#ifdef Q_OS_DARWIN
    char buf[512] = "";
        //qDebug()<<"************************************";
        //qDebug()<<"checkSetting";
        //qDebug()<<"************************************";
        get_platform_uuid(buf, sizeof(buf));
        //qDebug()<<"1\n";
        QString uuid = QString::fromUtf8(buf);
        //qDebug()<<"2\n";
        QString screenSaverConfPath = QDir::homePath()+"/Library/Preferences/ByHost/com.apple.screensaver."+uuid+".plist";
        //qDebug()<<"3\n";
        QSettings settings(screenSaverConfPath, QSettings::NativeFormat);
        //qDebug()<<"4\n";
        QMap<QString, QVariant> qm = settings.value("moduleDict").toMap();
        //qDebug()<<"5\n";


        QString screenSaverName = "";

        try{
            screenSaverName = qm.value("moduleName").toString();
            qDebug()<<"SCREENSAVERNAME: "<<screenSaverName<<" ";

        }catch(...){
            qDebug()<<"error when trying to get the modlule name";
        }

        QString screenSaverIdleTime ="";

        try{

            screenSaverIdleTime = settings.value("idleTime").toString();

        }catch(...){
            qDebug()<<"error wehn tryignt o get the idleTime name";

        }

        qDebug()<<"idleTime: "<< screenSaverIdleTime<<"\n";
        qDebug()<<"screenSaverName: "<<screenSaverName<<"\n";
#endif
#ifdef Q_OS_WIN32
    QSettings settings("HKEY_CURRENT_USER\\Control Panel\\Desktop", QSettings::NativeFormat);
    QString screenSaverIdleTime = settings.value("ScreenSaveTimeOut").toString();
    QString screenSaverName = settings.value("SCRNSAVE.EXE",NULL).toString();
    qDebug()<<"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"<<screenSaverIdleTime<<"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^";
    qDebug()<<"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"<<screenSaverName<<"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^";

#endif




        QString res = "";
        if (screenSaverIdleTime != "300"){
            res += "WTC";
            qDebug()<<"\nWait time changed\n";
        }

        //TODO: Check here if the screen saver is active.

        if (screenSaverName != "EcoScreenCatcherScreenSaver" && ! screenSaverName.endsWith("EcoScreenCatcher.scr")){
            if(res == ""){
                res += "SSC";
            }else{
                res += "_SSC";


            }

        //TODO: put conistend error reporting here...
        }

        if(res !=""){
            emit wrongConfiguration(res);
            qDebug()<<"\nEmittiiiiiiiiiiiiiiing res: "<<res;
        }
    }catch(...){

        qDebug()<<"Error with regards to checking if the user configured the configuration correctly. ";
    }





}


