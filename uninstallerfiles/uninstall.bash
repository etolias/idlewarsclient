#!/usr/bin/env bash

function die()
{
echo $*
exit 1
}

function cleanAndDie()
{
clean
die “IdleWars06 is already uninstalled”
}

function clean()
{
#local plist=~/Library/LaunchAgents/org.lastbamboo.littleshoot.plist
#test -f $plist && launchctl unload $plist
rm -rf /Applications/IdleWars*.app
#rm -f ~/Library/LaunchAgents/org.lastbamboo.littleshoot.plist
rm -rf /Library/Scripts/setscreensaver.sh
#rm -rf /System/Library/Screen Savers/IdleWarsScreenSaver.saver

#rm -rf ~/Library/Receipts/littleshoot.pkg
#rm -rf ~/.littleshoot
#rm -rf ~/Applications/LittleShootUninstaller.app
}

function remove()
{
rm -rf $1 || die “Could not remove file: $1″
}

# If it looks like we’ve already uninstalled, just make sure to remove everything again and die.
test -e /Applications/IdleWars*.app || cleanAndDie

#launchctl stop org.lastbamboo.littleshoot || die “Could not stop LittleShoot”
#launchctl unload ~/Library/LaunchAgents/org.lastbamboo.littleshoot.plist || die “Could not unload”
rm -rf /Applications/IdleWars*.app || die “Could not remove IdleWars06”
rm -rf /Library/Scripts/setscreensaver.sh
rm -rf /System/Library/Screen\ Savers/IdleWarsScreenSaver.saver
#rm -f ~/Library/LaunchAgents/org.lastbamboo.littleshoot.plist || die “Could not remove plist”

# We go through all this because the package file is placed differently on Tiger, Leopard, etc.
#globalReceipt=/Library/Receipts/littleshoot.pkg
#userReceipt=~/Library/Receipts/littleshoot.pkg
#test -e $globalReceipt && remove $globalReceipt
#test -e $localReceipt && remove $localReceipt
#rm -rf ~/.littleshoot || die “Could not remove LittleShoot config folder”
#rm -rf ~/Applications/LittleShootUninstaller.app || die “Could not remove LittleShoot uninstaller”

commandParta="'tell application \"System Events\" to delete login item \"$i\"'"
commandPartb="\"'"
for i in `osascript -e 'tell application "System Events" to get the name of every login item' | tr , '\n' | grep "IdleWars*"`
do
	commandParta="tell application \"System Events\" to delete login item \"\""
	
	FLAGS=(-e "tell application \"System Events\" to delete login item \"$i\"")
	osascript "${FLAGS[@]}"
	
done
###########
# 
# Get the Universally Unique Identifier (UUID) for the correct platform
# ioreg commands found in a comment at http://www.afp548.com/article.php?story=leopard_byhost_changes
#
###########

	# Check if hardware is PPC or early Intel
	if [[ `ioreg -rd1 -c IOPlatformExpertDevice | grep -i "UUID" | cut -c27-50` == "00000000-0000-1000-8000-" ]]; then
		macUUID=`ioreg -rd1 -c IOPlatformExpertDevice | grep -i "UUID" | cut -c51-62 | awk {'print tolower()'}`
	# Check if hardware is new Intel
	elif [[ `ioreg -rd1 -c IOPlatformExpertDevice | grep -i "UUID" | cut -c27-50` != "00000000-0000-1000-8000-" ]]; then
		macUUID=`ioreg -rd1 -c IOPlatformExpertDevice | grep -i "UUID" | cut -c27-62`
	fi

###########
rm -rf /System/Library/Screen\ Savers/IdleWarsScreenSaver.saver
rm -rf /Library/Screen\ Savers/IdleWarsScreenSaver.saver
#sh -c "sudo -u $loggedInUser /usr/bin/defaults -currentHost delete ~/Library/Preferences/ByHost/com.apple.screensaver.613E406D-5E9B-59F9-957B-04AF3910B051.plist moduleDict"
#sh -c "sudo -u $loggedInUser /usr/bin/defaults -currentHost delete ~/Library/Preferences/ByHost/com.apple.screensaver.613E406D-5E9B-59F9-957B-04AF3910B051.plist moduleName"
#sh -c "sudo -u $loggedInUser /usr/bin/defaults -currentHost delete ~/Library/Preferences/ByHost/com.apple.screensaver.613E406D-5E9B-59F9-957B-04AF3910B051.plist modulePath"

defaults delete ~/Library/Preferences/ByHost/com.apple.screensaver."$macUUID".plist moduleDict
defaults delete ~/Library/Preferences/ByHost/com.apple.screensaver."$macUUID".plist moduleName
defaults delete ~/Library/Preferences/ByHost/com.apple.screensaver."$macUUID".plist modulePath


#sh -c "sudo -u $loggedInUser /usr/bin/defaults -currentHost write com.apple.screensaver idleTime 0"
#sh -c "sudo -u $loggedInUser /usr/bin/defaults -currentHost write ~/Library/Preferences/ByHost/com.apple.screensaver.613E406D-5E9B-59F9-957B-04AF3910B051.plist idleTime 0"
#sh -c "sudo -u $loggedInUser /usr/bin/defaults -currentHost write ~/Library/Preferences/ByHost/com.apple.screensaver.613E406D-5E9B-59F9-957B-04AF3910B051.plist moduleName Flurry"



screenSaverPath="/System/Library/Screen\ Savers/Flurry.saver"		# String
screenSaverName="Flurry"		# String

if [[ -n $screenSaverName ]]; then
	/usr/libexec/PlistBuddy -c "Add :moduleDict:moduleName string $screenSaverName" "$loggedInUserHome"/Library/Preferences/ByHost/com.apple.screensaver."$macUUID".plist
	/usr/libexec/PlistBuddy -c "Add :moduleName string $screenSaverName" "$loggedInUserHome"/Library/Preferences/ByHost/com.apple.screensaver."$macUUID".plist
fi
if [[ -n $screenSaverPath ]]; then
	/usr/libexec/PlistBuddy -c "Add :moduleDict:path string $screenSaverPath" "$loggedInUserHome"/Library/Preferences/ByHost/com.apple.screensaver."$macUUID".plist
	/usr/libexec/PlistBuddy -c "Add :modulePath string $screenSaverPath" "$loggedInUserHome"/Library/Preferences/ByHost/com.apple.screensaver."$macUUID".plist
fi
osascript  -e 'tell application "IdleWars6.1" to quit'