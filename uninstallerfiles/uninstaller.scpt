on onConfirmUninstall()
set applicationName to "IdleWars"
try
display dialog "Are you sure you want to uninstall " & applicationName & "?"
set uninstallScript to quoted form of POSIX path of (path to resource "uninstall.bash")
do shell script "bash " & uninstallScript with administrator privileges
display dialog "Successfully Uninstalled " & applicationName buttons {"OK"} default button "OK"

on error err
if err contains "User canceled" then
display dialog "Canceled " & applicationName & " Uninstall" buttons {"OK"} default button "OK"
else
display dialog "We’re sorry, but there was an error uninstalling " & applicationName & " described as: " & err buttons {"OK"} default button "OK"
end if
end try
end onConfirmUninstall

onConfirmUninstall()