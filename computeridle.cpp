#include "computeridle.h"
#include "ui_computeridle.h"
#include <QImage>
#include <QLabel>
#include <QLineEdit>
#include <QGraphicsPixmapItem>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QDebug>

ComputerIdle::ComputerIdle(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ComputerIdle)
{

    ui->setupUi(this);
    this->setMouseTracking(true);
    qt=new QTimer(this);
    this->qt->setInterval(200);
    parent->connect(this->qt,SIGNAL(timeout()),this,SLOT(isMouseMoved()));
    qDebug() << "before starting";
    this->qt->start();
    qDebug() << "after starting";
    QPoint p = this->mapFromGlobal(QCursor::pos());
    this->prevX=-1;
    this->prevY=-1;

}

ComputerIdle::~ComputerIdle()
{
    delete ui;
}

void ComputerIdle::isMouseMoved(){
    QPoint p = this->mapFromGlobal(QCursor::pos());

    if (this->prevX==-1 && this->prevY==-1){
        this->prevX=p.x();
        this->prevY=p.y();
    }
    int curx=p.x();
    int cury=p.y();
    if (this->prevX!=curx || this->prevY!=cury){
        this->hide();
        emit userAction();
        //qDebug()<<"Mouse Moved action";
    }
    this->prevX=curx;
    this->prevY=cury;
}
void ComputerIdle::show_image_from_data(QByteArray img){
    qDebug() << "is full screen: " <<this->isFullScreen();
    qDebug() <<"is visible: " << this->isVisible();

    if(!this->isVisible()&& !this->isBusted){
        qDebug() << "show QR Code";
        this->setWindowFlags(Qt::CustomizeWindowHint);
        QImage image;
        image.loadFromData(img);
        QGraphicsPixmapItem* item = new QGraphicsPixmapItem(QPixmap::fromImage(image));
        QGraphicsView * gview= findChild<QGraphicsView*>("graphicsView");
        QGraphicsScene* scene = new QGraphicsScene();

        scene->addItem(item);

        gview->setScene(scene);


        this->showMaximized();
        //QLabel * label= findChild<QLabel*>("messageLabel");
        //label->setText("");
        //QLineEdit * pass= findChild<QLineEdit*>("passwordLineEdit");
        //pass->setText("");
        this->showFullScreen();
        this->raise();
        this->activateWindow();
        gview->fitInView(item,Qt::KeepAspectRatio);
        QPoint p = this->mapFromGlobal(QCursor::pos());
        qDebug() << "p.x()="<<p.x();
        qDebug()<< "p.y()="<<p.y();

    }
}
